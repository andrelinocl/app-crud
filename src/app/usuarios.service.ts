import { Injectable } from '@angular/core';
import { Usuario } from "app/usuario";

@Injectable()
export class UsuariosService {
  usuarios: Usuario[] = [
    {nome:"Fulano da Silva", 
      email:"fulano@mail.com", 
      username:"fulano"},    
    {nome:"Sicrano da Silva", 
      email:"sicrano@mail.com", 
      username:"sicrano"},    
    {nome:"Beltrano da Silva", 
      email:"beltrano@mail.com", 
      username:"beltrano"},    

  ];

  constructor() { 
  }

  getUsuarios(){
    return this.usuarios;
  }

  addUsuario(usuario){

    this.usuarios.push(usuario);
    
  }

  excluirUsuario(indice:number){
    this.usuarios.splice(indice,1);
  }

  getUsuario(indice:number){
    return(this.usuarios[indice]);
  }

  atualizarUsuario(indice:number, usuario:Usuario){
    this.usuarios[indice] = usuario;
  }
}
