import { Component, OnInit } from '@angular/core';
import { Usuario } from "app/usuario";
import { UsuariosService } from "app/usuarios.service";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: 'app-form-usuarios',
  templateUrl: './form-usuarios.component.html',
  styleUrls: ['./form-usuarios.component.css']
})
export class FormUsuariosComponent implements OnInit {
  usuario:Usuario;
  indice: number;
  constructor(private servico: UsuariosService ,
              private router: Router,
              private rota:ActivatedRoute) { 
  }

  ngOnInit() {
    this.indice = this.rota.snapshot.params['ind'];

    if(isNaN(this.indice)){
      this.usuario = new Usuario();    
    }
    else{
      this.usuario = Object.assign({},
          this.servico.getUsuario(this.indice));
    }

    
  }

  salvar(){
    if(isNaN(this.indice)){
      this.servico.addUsuario(this.usuario);
      this.usuario = new Usuario();    
    }
    else{
      this.servico.atualizarUsuario(
        this.indice,
        this.usuario
      );
    }
        this.router.navigate(['/lista']);
  }

}
