import { Component, OnInit } from '@angular/core';
import { Usuario } from "app/usuario";
import { UsuariosService } from "app/usuarios.service";

@Component({
  selector: 'app-table-usuarios',
  templateUrl: './table-usuarios.component.html',
  styleUrls: ['./table-usuarios.component.css']
})
export class TableUsuariosComponent implements OnInit {
  usuarios: Usuario[];

  constructor(private servico: UsuariosService ) { 
    
  }

  ngOnInit() {
    
    
    this.usuarios = this.servico.getUsuarios();
  }

  excluirUsuario(indice:number){
    this.servico.excluirUsuario(indice);
  }

}
